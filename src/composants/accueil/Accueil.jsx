import React from "react";
import style from "./Accueil.module.scss";
import CustomMapComponent from "../map/CustomMapComponent ";

const Accueil = ({ capitals }) => {

  return (
    <div className={style.mapAccueil}>
      <h1>Capitales mondiale</h1>
      <CustomMapComponent capitals={capitals}/>
    </div>
  );
};

export default Accueil;
