import React, { useEffect, useRef, useState } from "react";
import style from "./CustomMap.module.scss";
import {
  MapContainer as LeafletMap,
  TileLayer,
  Marker,
  Popup,
} from "react-leaflet";
import "leaflet/dist/leaflet.css";
import L from "leaflet";
import icon from "leaflet/dist/images/marker-icon.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";
import { useNavigate } from "react-router-dom";

const CustomMapComponent = ({ capitals: propCapitals, updateCity }) => {
  const navigate = useNavigate();
  const [userPosition, setUserPosition] = useState(null);
  const mapRef = useRef(null);
  const [localCapitals, setLocalCapitals] = useState([]);

  const DefaultIcon = L.icon({
    iconUrl: icon,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowUrl: iconShadow,
    shadowSize: [41, 41],
  });

  L.Marker.prototype.options.icon = DefaultIcon;

  useEffect(() => {
    // Récupérer les données depuis le localStorage
    const localStorageCapitals = JSON.parse(localStorage.getItem("capitals"));

    if (localStorageCapitals) {
      setLocalCapitals(localStorageCapitals);
    } else {
      // Si le localStorage est vide, utilisez les données du fichier JSON initial
      // Vous pouvez charger le fichier JSON comme vous le faisiez auparavant
      // capitalsData peut être importé depuis le fichier JSON initial
      setLocalCapitals(propCapitals);
    }
  }, [propCapitals]);

  const handleZoomToPosition = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const { latitude, longitude } = position.coords;
          setUserPosition({ lat: latitude, lng: longitude });

          if (mapRef.current) {
            mapRef.current.setView([latitude, longitude], 12);
          }
        },
        (error) => console.error(error),
        { enableHighAccuracy: true }
      );
    } else {
      console.error(
        "La géolocalisation n'est pas prise en charge par votre navigateur."
      );
    }
  };

  return (
    <div className={style.customMap}>
      <LeafletMap
        className={style.map}
        style={{ height: "600px" }}
        center={[0, 0]}
        zoom={2}
        ref={mapRef}
      >
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution="© OpenStreetMap contributors"
        />
        {localCapitals &&
          Array.isArray(localCapitals) &&
          localCapitals.map((capital) => (
            <Marker key={capital.id} position={[capital.lat, capital.lng]}>
              <Popup>
                <div>
                  <strong>
                    {capital.city}, {capital.country}
                  </strong>
                </div>
                <div>Population: {capital.population}</div>
              </Popup>
            </Marker>
          ))}
        {userPosition && (
          <Marker position={[userPosition.lat, userPosition.lng]}>
            <Popup>Votre position</Popup>
          </Marker>
        )}
      </LeafletMap>
      <div className={style.buttonDown}>
        <button onClick={handleZoomToPosition}>Ma Position</button>
        <button onClick={() => navigate("/editlist")}>Modifier</button>
      </div>
    </div>
  );
};

export default CustomMapComponent;
