import React, { useState, useEffect } from "react";
import style from "./TownList.module.scss";
import { useNavigate } from "react-router-dom";

const TownList = ({ capitals, updateCity }) => {
  const navigate = useNavigate();
  const [capitalsList, setCapitalsList] = useState(capitals);
  const [editedCity, setEditedCity] = useState({
    id: null,
    country: "",
    city: "",
    population: "",
    lat: "",
    lng: "",
    isEditing: false,
  });

  useEffect(() => {
    const localStorageCapitals = JSON.parse(localStorage.getItem("capitals"));

    if (localStorageCapitals) {
      setCapitalsList(localStorageCapitals);
    } else {
      setCapitalsList(capitals);
    }
  }, [capitals]);

  const handleEditClick = (id, currentCity) => {
    setEditedCity({
      ...currentCity,
      isEditing: true,
    });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setEditedCity((prevEditedCity) => ({ ...prevEditedCity, [name]: value }));
  };

  const handleDeleteClick = (id) => {
    const updatedCapitals = capitalsList.filter((city) => city.id !== id);
    setCapitalsList(updatedCapitals);

    localStorage.setItem("capitals", JSON.stringify(updatedCapitals));

    updateCity(id, null);
  };

  const handleSaveClick = (id) => {
    const updatedCapitals = capitalsList.map((city) =>
      city.id === id ? { ...city, ...editedCity, isEditing: false } : city
    );
    setCapitalsList(updatedCapitals);

    localStorage.setItem("capitals", JSON.stringify(updatedCapitals));

    updateCity(id, editedCity);
  
    setEditedCity({
      id: null,
      country: "",
      city: "",
      population: "",
      lat: "",
      lng: "",
      isEditing: false,
    });
  };

  const handleCancelClick = () => {
    setEditedCity({
      id: null,
      country: "",
      city: "",
      population: "",
      lat: "",
      lng: "",
      isEditing: false,
    });
  };
  
  

  const handleAddClick = () => {
    const newId = Math.max(...capitalsList.map((city) => city.id), 0) + 1;

    const newCity = {
      id: newId,
      country: editedCity.country,
      city: editedCity.city,
      population: editedCity.population,
      lat: editedCity.lat,
      lng: editedCity.lng,
    };

    const updatedCapitals = [...capitalsList, newCity];
    setCapitalsList(updatedCapitals);

    localStorage.setItem("capitals", JSON.stringify(updatedCapitals));
    updateCity(newId, newCity);
    setEditedCity({
      id: null,
      country: "",
      city: "",
      population: "",
      lat: "",
      lng: "",
      isEditing: false,
    });
  };

  return (
    <div className={style.listCity}>
      <button onClick={() => navigate("/")}>Retour</button>
      <h1>Liste des capitales</h1>
      <div className={style.editDeleteBlock}>
        <ul className={style.puce}>
          {capitalsList.map((capital) => (
            <li key={capital.id}>
              {editedCity.isEditing && editedCity.id === capital.id ? (
                <>
                  <div>
                    <input
                      type="text"
                      name="country"
                      placeholder="Country"
                      value={editedCity.country}
                      onChange={handleChange}
                    />
                  </div>
                  <div>
                    <input
                      type="text"
                      name="city"
                      placeholder="City"
                      value={editedCity.city}
                      onChange={handleChange}
                    />
                  </div>
                  <div>
                    <input
                      type="text"
                      name="population"
                      placeholder="Population"
                      value={editedCity.population}
                      onChange={handleChange}
                    />
                  </div>
                  <div>
                    <input
                      type="text"
                      name="lat"
                      placeholder="Latitude"
                      value={editedCity.lat}
                      onChange={handleChange}
                    />
                  </div>
                  <div>
                    <input
                      type="text"
                      name="lng"
                      placeholder="Longitude"
                      value={editedCity.lng}
                      onChange={handleChange}
                    />
                  </div>
                  <div>
                    <button className={style.save} onClick={() => handleSaveClick(capital.id)}>
                      Save
                    </button>
                    <button className={style.cancel} onClick={handleCancelClick}>Cancel</button>
                  </div>
                </>
              ) : (
                <>
                  <span>{`${capital.city}, ${capital.country}`}</span>
                  <div>
                    <button
                      onClick={() => handleEditClick(capital.id, capital)}
                    >
                      Informations
                    </button>
                    <button className={style.cancel} onClick={() => handleDeleteClick(capital.id)}>
                      Supprimer
                    </button>
                  </div>
                </>
              )}
            </li>
          ))}
        </ul>
      </div>
      <div className={style.addCity}>
        <h2>Ajout d'une nouvelle capitale</h2>
        <div>
          <input
            type="text"
            name="country"
            placeholder="Country"
            value={editedCity.country}
            onChange={handleChange}
          />
        </div>
        <div>
          <input
            type="text"
            name="city"
            placeholder="City"
            value={editedCity.city}
            onChange={handleChange}
          />
        </div>
        <div>
          <input
            type="text"
            name="population"
            placeholder="Population"
            value={editedCity.population}
            onChange={handleChange}
          />
        </div>
        <div>
          <input
            type="text"
            name="lat"
            placeholder="Latitude"
            value={editedCity.lat}
            onChange={handleChange}
          />
        </div>
        <div>
          <input
            type="text"
            name="lng"
            placeholder="Longitude"
            value={editedCity.lng}
            onChange={handleChange}
          />
        </div>
        <button className={style.save} onClick={handleAddClick}>Ajouter une capitale</button>
      </div>
    </div>
  );
};

export default TownList;
