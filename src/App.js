import React, { useState } from 'react';
import { Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';
import capitalsData from './data/capitals.json';
import Accueil from './composants/accueil/Accueil';
import EditList from './composants/townList/TownList';
import CustomMapComponent from './composants/map/CustomMapComponent ';

function App() {
  const [capitals, setCapitals] = useState(capitalsData);

  const updateCity = (id, updatedCity) => {
    
    setCapitals((prevCapitals) =>
      prevCapitals.map((city) => (city.id === id ? { ...city, ...updatedCity } : city))
    );
  };

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Routes>
        <Route
          path="/"
          element={
            <Accueil capitals={capitals}>
              <CustomMapComponent propCapitals={capitals} updateCity={updateCity} />
            </Accueil>
          }
        />
        <Route path="/editlist" element={<EditList capitals={capitals} updateCity={updateCity}/>} />
      </Routes>
    </Suspense>
  );
}

export default App;


